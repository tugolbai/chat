import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewMessageComponent } from './messages/new-message/new-message.component';
import { MessagesComponent } from './messages/messages.component';

const routes: Routes = [
  {path: '', component: MessagesComponent},
  {path: 'messages/new', component: NewMessageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
