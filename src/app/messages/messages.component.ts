import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../shared/message.model';
import { Subscription } from 'rxjs';
import { MessageService } from '../shared/message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messages: Message[] = [];
  messagesChangeSubscription!: Subscription;
  messagesFetchingSubscription!: Subscription;
  isFetching = false;

  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.messages = this.messageService.getMessages();
    this.messagesChangeSubscription = this.messageService.messagesChange.subscribe((messages: Message[]) => {
      this.messages = messages;
    });
    this.messagesFetchingSubscription = this.messageService.messagesFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.messageService.start();
  }

  ngOnDestroy() {
    this.messagesChangeSubscription.unsubscribe();
    this.messagesFetchingSubscription.unsubscribe();
  }

}
