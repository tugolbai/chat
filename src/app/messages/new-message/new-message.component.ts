import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MessageService } from '../../shared/message.service';
import { Router } from '@angular/router';
import { Message } from '../../shared/message.model';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit, OnDestroy {
  @ViewChild('f') messageForm!: NgForm;
  isUploading = false;
  messageUploadingSubscription!: Subscription;

  constructor(
    private messageService: MessageService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.messageUploadingSubscription = this.messageService.messageUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  addMessage() {
    const id =  Math.random().toString();

    const message = new Message(
      id,
      this.messageForm.value.author,
      this.messageForm.value.message,
      id
    );

    const next = () => {
      this.messageService.start();
      void this.router.navigate(['/']);
    };

    this.messageService.addMessage(message).subscribe(next);
  }

  ngOnDestroy(): void {
    this.messageUploadingSubscription.unsubscribe();
  }
}
