import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Message } from './message.model';

@Injectable()
export class MessageService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();
  messageUploading = new Subject<boolean>();

  private messages: Message[] = [];

  constructor(private http: HttpClient) {}

  getMessages() {
    return this.messages.slice();
  }

  start() {
    this.messagesFetching.next(true);
    this.http.get<[Message]>('http://146.185.154.90:8000/messages')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return result;
      }))
      .subscribe(messages => {
        this.messages = messages;
        this.messagesChange.next(this.messages.slice());
        this.messagesFetching.next(false);
      }, () => {
        this.messagesFetching.next(false);
      });
  }

  addMessage(message: Message) {
    const body = new HttpParams()
      .set('author', message.author)
      .set('message', message.message);
    this.messageUploading.next(true);

    return this.http.post('http://146.185.154.90:8000/messages', body).pipe(
      tap(() => {
        this.messageUploading.next(false);
      }, () => {
        this.messageUploading.next(false);
      })
    );
  }

}
